#include <stdio.h>
#include <string.h>
#include <stdbool.h>

bool win = 0;
bool berro = 0;

char temp;
char resp = 'S';
char board[3][3];
char p1 = 'X';
char p2 = 'O';
char patual;
char esc;

void flip(char *x, char *y);
void jogo(int i, int j);
void vitoria(void);

int erroc(char c);
int erro(int x);

void flip(char *x, char *y)
{
    char temp = *x;
    *x = *y;
    *y = temp;

    return;
}

void vitoria(void)
{
    for (int i = 0; i < 3; i++)
    {
        // Verificar vitória em linhas
        if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][1] == board[i][2])
        {
            win = 1;
            return;
        }

        // Verificar vitória em colunas
        if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[1][i] == board[2][i])
        {
            win = 1;
            return;
        }
    }

    // Verificar vitória na diagonal principal
    if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[1][1] == board[2][2])
    {
        win = 1;
        return;
    }

    // Verificar vitória na diagonal secundária
    if (board[2][0] != ' ' && board[2][0] == board[1][1] && board[1][1] == board[0][2])
    {
        win = 1;
        return;
    }
}

int erro(int x)
{
    if (x == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int erroc(char c)
{
    if (c == '\0')
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void jogo(int i, int j)
{
    i--;
    j--;

    if (board[i][j] == ' ')
    {
        board[i][j] = patual;

        printf("\n     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[0][0], board[0][1], board[0][2]);
        printf("     |     |      \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[1][0], board[1][1], board[1][2]);
        printf("     |     |     \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[2][0], board[2][1], board[2][2]);
        printf("     |     |     \n");

        if (win == 0)
        {
            printf("\nInsira as proximas coordenadas(linha e coluna)\n\n");
        }
    }
    else
    {
        printf("\nCasa invalida, escolha novamente.\n");

        printf("\n     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[0][0], board[0][1], board[0][2]);
        printf("     |     |      \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[1][0], board[1][1], board[1][2]);
        printf("     |     |     \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[2][0], board[2][1], board[2][2]);
        printf("     |     |     \n");
    }
}

int main()
{
    while (resp == 'S')
    {
        while (true)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    board[i][j] = ' ';
                }
            }

            scanf(" %c", &esc);

            if (esc != 'X' && esc != 'x' && esc != 'O' && esc != 'o')
            {
                printf("Caractere invalido, tente novamente\n");
            }
            else
            {
                break;
            }
        }
        if (esc == 'x')
        {
            p1 = 'x';
            p2 = 'o';
        }
        else if (esc == 'O')
        {
            flip(&p1, &p2);
        }
        else if (esc == 'o')
        {
            p1 = 'x';
            p2 = 'o';
            flip(&p1, &p2);
        }

        int casas[1];
        printf("Tudo bem, o '%c' vai comecar\n\n", p1);

        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[0][0], board[0][1], board[0][2]);
        printf("     |     |     \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[1][0], board[1][1], board[1][2]);
        printf("     |     |     \n");
        printf("----- ----- ----- \n");
        printf("     |     |     \n");
        printf("  %c  |  %c  |  %c  \n", board[2][0], board[2][1], board[2][2]);
        printf("     |     |     \n");

        printf("\nInsira as coordenadas para comecar(linha e coluna)\n\n");

        while (win == 0)
        {
            patual = p1;

            // Loop para permitir que o jogador 1 selecione uma casa válida
            while (true)
            {
                scanf("%d %d", &casas[0], &casas[1]);

                // Verificar se a casa selecionada é válida
                if (casas[0] > 0 && casas[0] <= 3 && casas[1] > 0 && casas[1] <= 3)
                {
                    break; // Sair do loop se a casa for válida
                }
                else
                {
                    printf("Casa invalida, escolha novamente.\n");
                }
            }

            // Marcar a casa selecionada no tabuleiro
            jogo(casas[0], casas[1]);

            // Verificar se houve uma vitória após a jogada do jogador 1
            vitoria();

            // Se houve uma vitória, sair do loop
            if (win != 0)
            {
                break;
            }

            patual = p2;

            // Loop para permitir que o jogador 2 selecione uma casa válida
            while (true)
            {
                scanf("%d %d", &casas[0], &casas[1]);

                // Verificar se a casa selecionada é válida
                if (casas[0] > 0 && casas[0] <= 3 && casas[1] > 0 && casas[1] <= 3)
                {
                    break; // Sair do loop se a casa for válida
                }
                else
                {
                    printf("Casa invalida, escolha novamente.\n");
                }
            }

            // Marcar a casa selecionada no tabuleiro
            jogo(casas[0], casas[1]);

            // Verificar se houve uma vitória após a jogada do jogador 2
            vitoria();
        }

        printf("%c ganhou!\nPressione S para jogar novamente e N para sair.\n", patual);
        while (temp != 'S')
        {
            scanf("%c", &temp);

            if (temp == 'S' || temp == 'N')
            {
                temp = resp;
                break;
            }

            else
            {
                printf("Resposta inválida, Respoda S para jogar novamente e N para sair do jogo!");
            }
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                board[i][j] = ' ';
            }
        }
    }
}